<?php

require_once "SQL.php";

/**
 * A class to use a simple CRUD application for Scrabble
 * @author Michael Thornley
 *
 */
class RoarClass
{
	/**
	 * Establish a DB connection, with the credentials just once
	 * @param unknown $hostname
	 * @param unknown $username
	 * @param unknown $password
	 * @param unknown $database
	 */
	public function __construct($hostname,$username,$password,$database)
	{
		/*Get the SQl class instance and connection once only*/
		if(!isset($this->mysqli))
		{
			$this->mysqli = new SQL($hostname,$username,$password,$database);
		}
	}
	
	/**
	 * Get all the members from the DB that have the enabled flag enabled=1
	 * @return stdClass of the members as an object.
	 */
	public function getAllMembers()
	{
		$this->query = 
		"
			SELECT 
				member_id AS id,
				CONCAT(first_name,' ',last_name) AS name,
				DATE_FORMAT(date_joined,'%d-%m-%Y') AS date_joined,
				contact_number
			FROM
				member
			WHERE enabled=1
			ORDER BY name 
		";
		
		$this->result = $this->mysqli->executeSQL($this->query);
		
		return $this->result;	
		
	}
		
	/**
	 * Get the member details from the passed in member_id
	 * @param unknown $getMember is the member_id passed in from read.php
	 */
	public function getMember($getMember)
	{
		if(!empty($getMember['id']))
		{
			$this->id = $this->mysqli->sanitiseVars($getMember['id']); 
			
			$this->query = 	
			'
			
				SELECT 
					COUNT(*) AS played
				FROM
					scores s
				WHERE
					s.member_id = "'.$this->id.'" AND (s.winner=0 OR s.winner=1)
			
			';
						
			$this->result = $this->mysqli->executeSQL($this->query);
					
			foreach($this->result as $result)
			{
				if($result->played==0)
				{
					return FALSE;
				}
			}
			
			$this->query = 	
			'
				SELECT 
					COUNT(*) AS `won`
				FROM
					scores
				WHERE
					member_id = "'.$this->id.'" AND winner = 1;
			';	
			
			$this->result = $this->mysqli->executeSQL($this->query);
			
			if(!isset($this->profileInfo))
			{
				$this->profileInfo = new stdClass;
			}
			
			foreach($this->result as $result)
			{
				$this->profileInfo->won = $result->won;
			}
					
			$this->query = 	
			'
				SELECT 
					COUNT(*) AS `lost`
				FROM
					scores
				WHERE
					member_id = "'.$this->id.'" AND winner = 0;
			';	
						
			$this->result = $this->mysqli->executeSQL($this->query);
			
			
			foreach($this->result as $result)
			{
				$this->profileInfo->lost = $result->lost;
			}
			
			$this->query = 	
			'
				SELECT 
					ROUND(AVG(score)) AS `average`
				FROM
					scores
				WHERE
					member_id = "'.$this->id.'";
			';	
						
			$this->result = $this->mysqli->executeSQL($this->query);
			
			
			foreach($this->result as $result)
			{
				$this->profileInfo->average = $result->average;
			}
			
			$this->query = 	
			'
				SELECT 
					CONCAT(first_name, " ", last_name) AS winner_name,
					s.game_id AS game_id,
					score AS maximum_score,
					DATE_FORMAT(when_played, "%d-%m-%Y") AS played_on,
					where_played
				FROM
					member m
						LEFT OUTER JOIN
					scores s ON s.member_id = m.member_id
						LEFT OUTER JOIN
					games g ON g.game_id = s.game_id
				WHERE
					m.member_id = "'.$this->id.'" AND winner = 1
				ORDER BY score DESC
				LIMIT 1
			';	
			
			$this->result = $this->mysqli->executeSQL($this->query);
			
			foreach($this->result as $result)
			{
				$this->profileInfo->game_id = $result->game_id;
				$this->profileInfo->maximum_score = $result->maximum_score;
				$this->profileInfo->played_on = $result->played_on;
				$this->profileInfo->where_played = $result->where_played;
				$this->profileInfo->winner_name = $result->winner_name;
				
			}
			
			// Get the losers name and their score
			$this->query = 	
			'
				SELECT 
					CONCAT(first_name," ", last_name) AS loser_name,
					s.score AS loser_score
				FROM
					scores s
						LEFT OUTER JOIN
					member m ON m.member_id = s.member_id
				WHERE
					game_id = "'.$this->profileInfo->game_id.'" AND winner = 0
			
			';
			
			$this->result = $this->mysqli->executeSQL($this->query);
			
			foreach($this->result as $result)
			{
				$this->profileInfo->loser_score = $result->loser_score;
				$this->profileInfo->loser_name = $result->loser_name;
			}
			
			return $this->profileInfo;
				
		}
		
		else
		{
			header('location: index.php');
		}
	}

	/**
	 * With the posted member details create the member
	 * @param unknown $memberDetails is the POSTED member details
	 * @throws exception if the query fails
	 */
	public function createMember($memberDetails)
	{
		if(!empty($memberDetails))
		{
			foreach($memberDetails AS $member=>$information)
			{
				if(!isset($this->memberAdd))
				{
				    $this->memberAdd = new stdClass;
				}
								
				$this->memberAdd->$member = $this->mysqli->sanitiseVars($information); 
			}
			
			$this->query = 
			'
				INSERT INTO member (first_name,last_name,date_joined,contact_number) 
				VALUES("'.$this->memberAdd->first_name.'","'.$this->memberAdd->last_name.'",NOW(),"'.$this->memberAdd->contact_number.'")	
			';	
			
			if(!$this->result = $this->mysqli->executeSQLNoData($this->query))
			{
				throw new exception("Query failed: " . $ex->getMessage());
			}
			
			else
			{
				header('location: index.php');
			}
		}
		
		else
		{
			return false;
		}
	}
	
	/**
	 * Get the original members details from the DB
	 * @param unknown $memberDetails is the member_id posted
	 * @throws exception if the query fails. 
	 */
	public function updateMember($memberDetails)
	{
		if(!empty($memberDetails['id']))
		{
			$this->id = $this->mysqli->sanitiseVars($memberDetails['id']); 
			
			$this->query = 	
			'
				SELECT *
					
				FROM
					member m
					
				WHERE
					m.member_id = "'.$this->id.'"
			';	
			
			if(!$this->result = $this->mysqli->executeSQL($this->query))
			{
				throw new exception("Query failed: " . $ex->getMessage());
			}
					
			foreach($this->result AS $details)
			{
				if(!isset($this->profileInfo))
				{
				    $this->profileInfo = new stdClass;
				}
				$this->profileInfo->member_id=$details->member_id;
				$this->profileInfo->first_name=$details->first_name;
				$this->profileInfo->last_name=$details->last_name;
				$this->profileInfo->contact_number=$details->contact_number;
				$this->profileInfo->date_joined=$details->date_joined;
			}
			
			return $this->profileInfo;
		}
	}
	/**
	 * Update the member details
	 * @param unknown $memberDetails are the POSTED member details from the form.php page
	 * @throws exception if the query fails. 
	 */
	public function updateMemberFinal($memberDetails)
	{
		if(!empty($memberDetails))
		{
			foreach($memberDetails AS $memberUpdate=>$information)
			{
				if(!isset($this->memberUpdate))
				{
				    $this->memberUpdate = new stdClass;
				}
								
				$this->memberUpdate->$memberUpdate = $this->mysqli->sanitiseVars($information); 
			}
			
			$this->query = 
			'
				UPDATE member 
					SET enabled=1, first_name="'.$this->memberUpdate->first_name.'",last_name="'.$this->memberUpdate->last_name.'", contact_number="'.$this->memberUpdate->contact_number.'" 
				WHERE member_id = "'.$this->memberUpdate->member_id.'"
			';	
			if(!$this->result = $this->mysqli->executeSQLNoData($this->query))
			{
				throw new exception("Query failed: " . $ex->getMessage());
			}
			
			else
			{
				header('location: index.php');
			}
		}
		
		else
		{
			return false;
		}
	}
	
	/**
	 * Get the leaderboard details on the highest scorer, who they played, where and when
	 * The 5 or greater than games they played we'll get on the front end. 
	 */
	public function getLeaderboard()
	{
		$this->query = 	
		'
			SELECT 
				CONCAT(first_name, " ", last_name) AS name,
				m.member_id AS id,
				ROUND(AVG(score)) AS average,
				COUNT(*) AS games,
				enabled
			FROM
				scores s
					LEFT OUTER JOIN
				member m ON m.member_id = s.member_id
			GROUP BY m.member_id
			HAVING
				/*Games greater than 5 and where they are still shown in the list*/
				m.enabled = 1 AND games>=5
			ORDER BY games DESC
		';
		
		$this->result = $this->mysqli->executeSQL($this->query);
		
		if(!$this->result)
		{
			return false;
		}
							
		foreach($this->result AS $details=>$information)
		{
			if(!isset($this->leaderBoard))
			{
				$this->leaderBoard = new stdClass;
			}
			
			$this->leaderBoard->$details=$information;
		
		}
			
		return $this->leaderBoard;
		
	}
	
	/**
	 * Disable the view of the member on the front end index.php page and set enabled in members to zero.
	 * @param unknown $delete is the ID of the member.
	 */
	public function deleteMember($delete)
	{
		$this->id = $this->mysqli->sanitiseVars($delete['id']); 
		$this->query = 'UPDATE member SET enabled=0 WHERE member_id= "'.$this->id.'"';
		$this->mysqli->executeSQLNoData($this->query);
		header('location: index.php');
	}
}
