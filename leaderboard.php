<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Scrabble CRUD application - update</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
	<body>
<?php require_once "roarclass.php";
	try
	{
		$roarClass = new RoarClass('localhost','mike','Staticmouse75!','scrabble');
		$leaderBoard = $roarClass->getLeaderboard();
	}

	catch(Exception $ex)
	{
		echo $ex->getMessage();
	}
?>

<div class="container">
	<?php if($leaderBoard!==false)
	{?>
		<h2>Leaderboard Top 5 averages on 5 games or more.</h2>
		</body>
		<table>
		<thead>
			<tr>
				<th>Name</th>
				<th>Average Score</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($leaderBoard AS $leaderBoardData)
		{?>	
			<tr>
				<td><?=$leaderBoardData->name?></td>
				<td><?=$leaderBoardData->average?></td>
				</tr>
			<?php
		}
	}
	else
	{?>
		<h2 class="notification">No leaderboard Information at this time.</h2>
	<?php
	}?>
	</tbody>
	</table>
	</body>
</html>
</div>