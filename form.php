<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Scrabble CRUD application - update</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<?php require_once "roarclass.php";
	try
	{
		$roarClass = new RoarClass('localhost','mike','Staticmouse75!','scrabble');
		
		/*
		If they navigate from index.php and want to create.
		If there are posted variables from the create form.
		*/
		if(isset($_POST['state']))
		{
			if($_POST['state']=='create')
			{
				$createMember = $roarClass->createMember($_POST);
			}
		}
		
		/*If there is to be an update, pull the original data first to populate the form.
		Then POST vars from the form on submit.
		*/
		if((isset($_GET['action']) && isset($_GET['id'])) || (isset($_POST['state'])))
		{
			if($_GET['action'] == "update" || $_POST['state']=='update')
			{
				$updateMember = $roarClass->updateMember($_GET);
				$updateMemberFinal = $roarClass->updateMemberFinal($_POST);
			}
		}

	}

	catch(Exception $ex)
	{
		echo $ex->getMessage();
	}
?>
	<div class="container">
		<div class="form">
			<h2><?=($_GET['action']=='update' && $_GET['id']) ? 'Update Profile' : 'Create Profile';?></h2>
				<form name="AddMember" method="POST" action="<?=$_SERVER['PHP_SELF']?>">
					<p>Name:<br/><input type="text" maxlength="25" name="first_name" value="<?php if(isset($updateMember->first_name)){ echo $updateMember->first_name;}?>" required></p>
					<p>Last name:<br/> <input type="text" maxlength="25" name="last_name" value="<?php if(isset($updateMember->last_name)){ echo $updateMember->last_name;}?>" required></p>
					<p>Contact number: <br/><input type="tel" maxlength="11" name="contact_number" value="<?php if(isset($updateMember->contact_number)){ echo $updateMember->contact_number;}?>" pattern="\d{11}" title="Phone Number (Format: eleven digits)" required/> </p>
					<p><input type='hidden' name="member_id" value="<?php if(isset($updateMember->member_id)){ echo $updateMember->member_id;}?>"/></p>
					<p><input type='hidden' name="state" value="<?=($_GET['action']=='update' && $_GET['id']) ? 'update' : 'create';?>"/></p>
					<p><input type="submit"/></p>
				</form>
			</body>
		</div>
	</div>
</body>
</html>