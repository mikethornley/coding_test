<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Scrabble CRUD application - read</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<?php require_once "roarclass.php";
	try
	{
		$roarClass = new RoarClass('localhost','mike','Staticmouse75!','scrabble');
		$memberHistory = $roarClass->getMember($_GET);
	}

	catch(Exception $ex)
	{
		echo $ex->getMessage();
	}
?>

<div class="container">
	<?php if($memberHistory!==false)
	{?>
	<h2> Member details for  <?=$memberHistory->winner_name?> </h2>
	<table>
		<tr>
			<th>Lost</th>
			<td><?=$memberHistory->lost?></td>
		</tr>
		<tr>
			<th>Wins</th>
			<td><?=$memberHistory->won?></td>
		</tr>
		<tr>
			<th>Average Score</th>
			<td><?=$memberHistory->average?></td>
		</tr>
		<tr>
			<th>Highest Score</th>
			<td>Scored: <?=$memberHistory->maximum_score?>, playing on <?=$memberHistory->played_on?> in <?=$memberHistory->where_played?> against <?=$memberHistory->loser_name?> scoring <?=$memberHistory->loser_score?> </td>
		</tr>
	</table>
	<?php
	}
	else
	{?>
		<h2 class="notification">Member hasn't played any matches yet</h2>
	<?php
	}?>
</div>
